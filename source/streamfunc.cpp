#include "grid.h"
#include "conjugategrad.h"
#include "pcgsolver.h"

using namespace std;

namespace Manta {

KERNEL() void ip_cent2mac(const Grid<Vec3> &p_in, MACGrid &p_out)
{
	if(i+1>=p_in.getSizeX() || j+1>=p_in.getSizeY() || k+1>=p_in.getSizeZ()) return;
	p_out(i,j,k).x = (p_in(i,j,k).x + p_in(i+1,j,k).x) * .5;
	p_out(i,j,k).y = (p_in(i,j,k).y + p_in(i,j+1,k).y) * .5;
	p_out(i,j,k).z = (p_in(i,j,k).z + p_in(i,j,k+1).z) * .5;
}

KERNEL() void ip_mac2cent(const MACGrid &p_in, Grid<Vec3> &p_out)
{
	if(i+1>=p_in.getSizeX() || j+1>=p_in.getSizeY() || k+1>=p_in.getSizeZ()) return;
	p_out(i,j,k).x = (p_in(i,j,k).x + p_in(i+1,j,k).x) * .5;
	p_out(i,j,k).y = (p_in(i,j,k).y + p_in(i,j+1,k).y) * .5;
	p_out(i,j,k).z = (p_in(i,j,k).z + p_in(i,j,k+1).z) * .5;
}

KERNEL() void ip_corners2mac(const Grid<Vec3> &p_in, MACGrid &p_out)
{
	if(i+1>=p_in.getSizeX() || j+1>=p_in.getSizeY() || k+1>=p_in.getSizeZ()) return;

    Real inter1 = (p_in(i,j,k).x   + p_in(i,j,k+1).x)   *.5;
    Real inter2 = (p_in(i,j+1,k).x + p_in(i,j+1,k+1).x) *.5;
    p_out(i,j,k).x = (inter1+inter2)*.5;

    inter1 = (p_in(i,j,k).y   + p_in(i,j,k+1).y)   * .5;
    inter2 = (p_in(i+1,j,k).y + p_in(i+1,j,k+1).y) * .5;
    p_out(i,j,k).y = (inter1+inter2)*.5;

    inter1 = (p_in(i,j,k).z   + p_in(i+1,j,k).z)   * .5;
    inter2 = (p_in(i,j+1,k).z + p_in(i+1,j+1,k).z) * .5;
    p_out(i,j,k).z = (inter1+inter2)*.5;
}

PYTHON() void interpol_corners2mac(const Grid<Vec3> &p_in, MACGrid &p_out)
{
	ip_corners2mac(p_in, p_out);
}
KERNEL() void copy_rhs(const Grid<Vec3> &p_omega, Grid<Real>& p_rhs, const int &p_iter)
{
	switch(p_iter)
	{
		case 0:
			p_rhs(i,j,k) = p_omega(i,j,k).x;
			break;
		case 1:
			p_rhs(i,j,k) = p_omega(i,j,k).y;
			break;
		case 2:
			p_rhs(i,j,k) = p_omega(i,j,k).z;
			break;
	}
}

KERNEL() void copy_out(const Grid<Real>& p_out, Grid<Vec3> &p_stream, const int &p_iter)
{
	switch(p_iter)
	{
		case 0:
			p_stream(i,j,k).x = p_out(i,j,k);
			break;
		case 1:
			p_stream(i,j,k).y = p_out(i,j,k);
			break;
		case 2:
			p_stream(i,j,k).z = p_out(i,j,k);
			break;
	}
}
KERNEL() void extrapolate_stream(Grid<Vec3> &p_stream, const int &p_dummy)
{
	if(i==p_stream.getSizeX()-1) p_stream(i,j,k) = p_stream(i-1,j,k);
	if(j==p_stream.getSizeY()-1) p_stream(i,j,k) = p_stream(i,j-1,k);
	if(k==p_stream.getSizeZ()-1) p_stream(i,j,k) = p_stream(i,j,k-1);
	if(i==0) p_stream(i,j,k) = p_stream(i+1,j,k);
	if(j==0) p_stream(i,j,k) = p_stream(i,j+1,k);
	if(k==0) p_stream(i,j,k) = p_stream(i,j,k+1);
}
KERNEL(reduce=+) returns(double sum=0)
double compute_sum(Grid<Real>& p_grid, const int &dummy)
{
	sum += p_grid(i,j,k);
}
KERNEL() void sub_avg(Grid<Real>& p_grid, const Real& p_avg)
{
	p_grid(i,j,k) -= p_avg;
}

void make_rhs(const Grid<Vec3> &p_omega, Grid<Real>& p_rhs, const int &p_iter)
{
	copy_rhs(p_omega, p_rhs, p_iter);

	double sum = compute_sum(p_rhs, 0);
	Real avg = sum / (p_rhs.getSizeX()*p_rhs.getSizeY()*p_rhs.getSizeZ());
	sub_avg(p_rhs, avg);
	sum = compute_sum(p_rhs, 0);
	std::cout << "rhs sum: " << sum << std::endl;
}

KERNEL ()
void MakeStreamMatrix(FlagGrid& flags, Grid<Real>& A0, Grid<Real>& Ai, Grid<Real>& Aj, Grid<Real>& Ak, MACGrid* fractions = 0) {

	if (i==0 || i == flags.getSizeX()-1 || j==0 || j==flags.getSizeY()-1 || k==0 || k==flags.getSizeZ()-1) {

        A0(i,j,k) = 1.0;
        return;
    }

    if (!flags.isFluid(i,j,k))
		return;

	if(!fractions) {
		// diagonal, A0
		if (!flags.isObstacle(i-1,j,k)) A0(i,j,k) += 1.;
		if (!flags.isObstacle(i+1,j,k)) A0(i,j,k) += 1.;
		if (!flags.isObstacle(i,j-1,k)) A0(i,j,k) += 1.;
		if (!flags.isObstacle(i,j+1,k)) A0(i,j,k) += 1.;
		if (flags.is3D() && !flags.isObstacle(i,j,k-1)) A0(i,j,k) += 1.;
		if (flags.is3D() && !flags.isObstacle(i,j,k+1)) A0(i,j,k) += 1.;

		// off-diagonal entries
		if (flags.isFluid(i+1,j,k)) Ai(i,j,k) = -1.;
		if (flags.isFluid(i,j+1,k)) Aj(i,j,k) = -1.;
		if (flags.is3D() && flags.isFluid(i,j,k+1)) Ak(i,j,k) = -1.;
	} else {
		// diagonal
		A0(i,j,k) += fractions->get(i,j,k).x;
		A0(i,j,k) += fractions->get(i+1,j,k).x;
		A0(i,j,k) += fractions->get(i,j,k).y;
		A0(i,j,k) += fractions->get(i,j+1,k).y;
		if (flags.is3D()) A0(i,j,k) += fractions->get(i,j,k).z;
		if (flags.is3D()) A0(i,j,k) += fractions->get(i,j,k+1).z;

		// off-diagonal entries
		Ai(i,j,k) = -fractions->get(i+1,j,k).x;
		Aj(i,j,k) = -fractions->get(i,j+1,k).y;
		if (flags.is3D()) Ak(i,j,k) = -fractions->get(i,j,k+1).z;
	}

}

PYTHON() void solve_stream_function(FlagGrid& p_flags, Grid<Vec3> &p_omega, Grid<Vec3> &p_stream)
{
    int nx = p_flags.getSizeX();
    int ny = p_flags.getSizeY();
    int nz = p_flags.getSizeZ();
    int n = nx*ny*nz;

	Real cgMaxIterFac = 1.5;
	Real cgAccuracy   = 1e-3;
	bool precondition = false;

	FluidSolver* parent = p_flags.getParent();

	Grid<Real> rhs(parent);
	Grid<Real> residual(parent);
	Grid<Real> search(parent);
	Grid<Real> A0(parent);
	Grid<Real> Ai(parent);
	Grid<Real> Aj(parent);
	Grid<Real> Ak(parent);
	Grid<Real> tmp(parent);
	Grid<Real> out(parent);
	Grid<Vec3> stream_tmp(parent);

	p_stream.clear();

    //MakeLaplaceMatrix (p_flags, A0, Ai, Aj, Ak);
	MakeStreamMatrix (p_flags, A0, Ai, Aj, Ak);

    //debug
    // for(int k = 0; k < nz; ++k) {
    //     for(int j = 0; j < ny; ++j) {
    //         for(int i = 0; i < nx; ++i) {
    //             //std::cout << "pcg diag: " << A(i,i) << std::endl;
    //             if(A0(i,j,k) != 0)
    //             {
    //                std::cout << i << " " << j << " " << k << " " << A0(i,j,k) << std::endl;
    //             }
    //         }
    //     }
    // }

	for(int i=0; i<3; ++i)
	{
		out.clear();
		rhs.clear();

		Grid<Real> pca0(parent);
		Grid<Real> pca1(parent);
		Grid<Real> pca2(parent);
		Grid<Real> pca3(parent);

		make_rhs(p_omega, rhs, i);

		const int maxIter = (int)(cgMaxIterFac * p_flags.getSize().max()) * (p_flags.is3D() ? 1 : 4);
		GridCgInterface *gcg;
		if (p_flags.is3D())
			gcg = new GridCg<ApplyMatrix  >(out, rhs, residual, search, p_flags, tmp, &A0, &Ai, &Aj, &Ak );
		else
			gcg = new GridCg<ApplyMatrix2D>(out, rhs, residual, search, p_flags, tmp, &A0, &Ai, &Aj, &Ak );

		gcg->setAccuracy( cgAccuracy );

		gcg->setICPreconditioner( precondition ? GridCgInterface::PC_mICP : GridCgInterface::PC_None, &pca0, &pca1, &pca2, &pca3);

		for (int iter=0; iter<maxIter; iter++) {
			if (!gcg->iterate()) iter=maxIter;
		}
		debMsg("FluidSolver::solveStreamiterations:"<<gcg->getIterations()<<", res:"<<gcg->getSigma(), 1);

		copy_out(out, p_stream, i);

		delete gcg;
	}
}

PYTHON() void solve_stream_function_pcg(FlagGrid& p_flags, Grid<Vec3> &p_omega, Grid<Vec3> &p_stream)
{
	int nx = p_flags.getSizeX();
	int ny = p_flags.getSizeY();
	int nz = p_flags.getSizeZ();
	int n = nx*ny*nz;

    // A matrix
    Manta::SparseMatrix<double> A(n);

    // b vector
    std::vector<double> bx(n);
    std::vector<double> by(n);
    std::vector<double> bz(n);

    // x vector
    std::vector<double> streamx(n);
    std::vector<double> streamy(n);
    std::vector<double> streamz(n);
    for (int i = 0; i < n; ++i) { streamx[i] = streamy[i] = streamz[i] = 0.; }

    Grid<Vec3> stream_tmp(p_flags.getParent());

    // handle rhs (make sum_rhs = 0)
    double sumx(0);
    double sumy(0);
    double sumz(0);
    for(int k = 0; k < nz; ++k) {
        for(int j = 0; j < ny; ++j) {
        	for(int i = 0; i < nx; ++i) {

                sumx += p_omega(i,j,k).x;
                sumy += p_omega(i,j,k).y;
                sumz += p_omega(i,j,k).z;

            }
        }
    }

    //offsets
    double offx = sumx/(double)n;
    double offy = sumy/(double)n;
    double offz = sumz/(double)n;

    for(int k = 0; k < nz; ++k) {
        for(int j = 0; j < ny; ++j) {
            for(int i = 0; i < nx; ++i) {

                int ijk = i+j*nx+k*nx*ny;

                if (i==0 || i == nx-1 || j==0 || j==ny-1 || k==0 || k==nz-1) {

                    A.set_element(ijk, ijk, 1.0);

                }else{

                    //diagonal
                    A.set_element(ijk, ijk, 6.0);

                    //off diagonals
                    if (i-1  >= 1) {
                        int index = (i-1)+j*nx+k*nx*ny;
                        // if(use_sdf == 1) {
                        //     int indexsdf = j+(i-1)*ny+k*nx*ny;
                        //     if( sdf[indexsdf] > 0 ) {A.set_element(index, ijk, -1.); A.set_element(ijk, index, -1.); }
                        // }else {
                        	A.set_element(index, ijk, -1.); A.set_element(ijk, index, -1.);
                        // }
                        //std::cout <<  i << " " << j << " " << k << " " << A(index, ijk) << std::endl;
                    }

                    if (i+1  < nx-1) {
                        int index = (i+1)+j*nx+k*nx*ny;
                        // if(use_sdf == 1) {
                        //     int indexsdf = j+(i+1)*ny+k*nx*ny;
                        //     if( sdf[indexsdf] > 0 ) {A.set_element(index, ijk, -1.); A.set_element(ijk, index, -1.); }
                        // }else {
                        	A.set_element(index, ijk, -1.); A.set_element(ijk, index, -1.);
                        // }
                        //std::cout << A(index, ijk) << std::endl;
                    }
                    if (j-1  >= 1) {
                        int index = i+(j-1)*nx+k*nx*ny;
                        // if(use_sdf == 1) {
                        //     int indexsdf = j-1+i*ny+k*nx*ny;
                        //     if( sdf[indexsdf] > 0 ) {A.set_element(index, ijk, -1.); A.set_element(ijk, index, -1.); }
                        // }else {
                        	A.set_element(index, ijk, -1.); A.set_element(ijk, index, -1.);
                        // }
                        //std::cout <<  i << " " << j << " " << k << " " << A(index, ijk) << std::endl;
                    }
                    if (j+1  < ny-1) {
                        int index = i+(j+1)*nx+k*nx*ny;
                        // if(use_sdf == 1) {
                        //     int indexsdf = j+1+i*ny+k*nx*ny;
                        //     if( sdf[indexsdf] > 0 ) {A.set_element(index, ijk, -1.); A.set_element(ijk, index, -1.); }
                        // }else {
                        	A.set_element(index, ijk, -1.); A.set_element(ijk, index, -1.);
                        // }
                    }
                    if (k-1  >= 1) {
                        int index = i+j*nx+(k-1)*nx*ny;
                        // if(use_sdf == 1) {
                        //     int indexsdf = j+i*ny+(k-1)*nx*ny;
                        //     if( sdf[indexsdf] > 0 ) {A.set_element(index, ijk, -1.); A.set_element(ijk, index, -1.); }
                        // }else {
                        	A.set_element(index, ijk, -1.); A.set_element(ijk, index, -1.);
                        // }
                        // std::cout <<  i << " " << j << " " << k << " " << A(index, ijk) << std::endl;
                    }
                    if (k+1  < nz-1) {
                        int index = i+j*nx+(k+1)*nx*ny;
                        // if(use_sdf == 1) {
                        //     int indexsdf = j+i*ny+(k+1)*nx*ny;
                        //     if( sdf[indexsdf] > 0 ) {A.set_element(index, ijk, -1.); A.set_element(ijk, index, -1.0); }
                        // }else {
                        	A.set_element(index, ijk, -1.); A.set_element(ijk, index, -1.);
                        // }
                    }

                }

                //rhs
                bx.at(ijk) = p_omega(i,j,k).x - offx;
                by.at(ijk) = p_omega(i,j,k).y - offy;
                bz.at(ijk) = p_omega(i,j,k).z - offz;
            }
        }
    }

    //debug
    // for(int k = 0; k < nz; ++k) {
    //     for(int j = 0; j < ny; ++j) {
    //         for(int i = 0; i < nx; ++i) {

    //             int ijk = i+j*nx+k*nx*ny;
    //             std::cout << i << " " << j << " " << k << " " << A(ijk,ijk) << std::endl;
    //         }
    //     }
    // }

    // setup solver
    Manta::SparsePCGSolver<double> solver;
    solver.set_solver_parameters(1e-3, 100, .97, 0.25);

    // params for printing
    int iter[3]={0};
    double resis[3]={0.0};

    // solve
    double res = 1e10;
    int  iters = -1;
    solver.solve(A, bx, streamx, res, iters, 2);
    iter[0] = iters; resis[0] = res;
    res = 1e10;iters = -1;
    solver.solve(A, by, streamy, res, iters, 2);
    iter[1] = iters; resis[1] = res;
    res = 1e10;iters = -1;
    solver.solve(A, bz, streamz, res, iters, 2);
    iter[2] = iters; resis[2] = res;
    std:: cout << "- pcg: x: i=" << iter[0] << " res=" << resis[0] <<"; y: i=" << iter[1] << " res=" << resis[1] <<"; z: i=" << iter[2] << " res=" << resis[2] << std::endl;

    for(int k = 0; k < nz; ++k) {
        for(int j = 0; j < ny; ++j) {
            for(int i = 0; i < nx; ++i) {

                int ijk = i+j*nx+k*nx*ny;

		        p_stream(i,j,k).x = streamx[ijk];
		        p_stream(i,j,k).y = streamy[ijk];
		        p_stream(i,j,k).z = streamz[ijk];
		    }
		}
    }

}

KERNEL() void kn_curl1(const MACGrid &p_in, Grid<Vec3> &p_curl)
{
	if(i == 0 || j == 0 || k == 0) return;

	p_curl(i,j,k).x = p_in(i,j,k).z - p_in(i,j-1,k).z - (p_in(i,j,k).y - p_in(i,j,k-1).y);
	p_curl(i,j,k).y = p_in(i,j,k).x - p_in(i,j,k-1).x - (p_in(i,j,k).z - p_in(i-1,j,k).z);
	p_curl(i,j,k).z = p_in(i,j,k).y - p_in(i-1,j,k).y - (p_in(i,j,k).x - p_in(i,j-1,k).x);
}

KERNEL() void kn_curl2(const Grid<Vec3> &p_in, MACGrid &p_curl)
{
	if(i==p_in.getSizeX()-1 || j==p_in.getSizeY()-1 || k==p_in.getSizeZ()-1) return;

	p_curl(i,j,k).x = p_in(i,j+1,k).z - p_in(i,j,k).z - (p_in(i,j,k+1).y - p_in(i,j,k).y);
	p_curl(i,j,k).y = p_in(i,j,k+1).x - p_in(i,j,k).x - (p_in(i+1,j,k).z - p_in(i,j,k).z);
	p_curl(i,j,k).z = p_in(i+1,j,k).y - p_in(i,j,k).y - (p_in(i,j+1,k).x - p_in(i,j,k).x);
}

KERNEL() void kn_fill_bound1(Grid<Vec3> &p_curl, const int dummy)
{
	if(i==0)
	{
		p_curl(i,j,k) = p_curl(i+1,j,k);
	}
	else if( j==0 )
	{
		p_curl(i,j,k) = p_curl(i,j+1,k);
	}
	else if(k==0)
	{
		p_curl(i,j,k) = p_curl(i,j,k+1);
	}
}

KERNEL() void kn_fill_bound2(MACGrid &p_curl, const int dummy)
{
	if(i==p_curl.getSizeX()-1)
	{
		p_curl(i,j,k) = p_curl(i-1,j,k);
	}
	else if( j==p_curl.getSizeY()-1 )
	{
		p_curl(i,j,k) = p_curl(i,j-1,k);
	}
	else if(k==p_curl.getSizeZ()-1)
	{
		p_curl(i,j,k) = p_curl(i,j,k-1);
	}
}

PYTHON() void curl1(const MACGrid &p_in, Grid<Vec3> &p_curl)
{
	kn_curl1(p_in,p_curl);
	kn_fill_bound1(p_curl,0);
}

PYTHON() void curl2(const Grid<Vec3> &p_in, MACGrid &p_curl)
{
	kn_curl2(p_in,p_curl);
	kn_fill_bound2(p_curl,0);
}


}

