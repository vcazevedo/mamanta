/******************************************************************************
*
* MantaFlow fluid solver framework
* Copyright 2011 Tobias Pfaff, Nils Thuerey
*
* This program is free software, distributed under the terms of the
* GNU General Public License (GPL)
* http://www.gnu.org/licenses
*
* FLIP (fluid implicit particles)
* for use with particle data fields
*
******************************************************************************/

#include "grid.h"
#include "commonkernels.h"

using namespace std;
namespace Manta {

void fnStreamFunction2D(FlagGrid& flags, MACGrid &vel, Grid<Real> &dst)
{
	// sequential process
	// dx = 1
	int bnd = 1;
	int k = 0;
	//Real dx = dst.getDx(); // to use velocity scale
	dst(bnd, bnd, k) = 0;
	for (int i = bnd + 1; i < dst.getSizeX() - bnd; ++i)
		dst(i, bnd, k) = -vel.getAtMACY(i - 1, bnd, k).y + dst(i - 1, bnd, k);

	for (int i = bnd; i < dst.getSizeX() - bnd; ++i)
		for (int j = bnd + 1; j < dst.getSizeY() - bnd; ++j)
			dst(i, j, k) = vel.getAtMACX(i, j - 1, k).x + dst(i, j - 1, k);

	//for (int j = bnd + 1; j < dst.getSizeY() - bnd; ++j)
	//	dst(bnd, j, k) = -vel.getAtMACX(bnd, j - 1, k).x + dst(bnd, j - 1, k);

	//for (int i = bnd+1; i < dst.getSizeX()-bnd; ++i)
	//	dst(i, bnd, k) = -vel.getAtMACY(i-1, bnd, k).y + dst(bnd, bnd, k);

	//for (int i = bnd+1; i < dst.getSizeX()-bnd; ++i)
	//	for (int j = bnd + 1; j < dst.getSizeY() - bnd; ++j)
	//		dst(i, j, k) = -vel.getAtMACX(i, j - 1, k).x + dst(i, j - 1, k);

	//for (int j = bnd+1; j < dst.getSizeY()-bnd; ++j)
	//	dst(bnd, j, k) = -vel.getAtMACX(bnd, j - 1, k).x + dst(bnd, j - 1, k);
}

void fnStreamFunction3D(FlagGrid& flags, MACGrid &vel, Grid<Real> &dst)
{
	// sequential process
	// dx = 1
	int bnd = 1;
	dst(bnd, bnd, bnd) = 0;
	for (int i = bnd + 1; i < dst.getSizeX() - bnd; ++i)
		dst(i, bnd, bnd) = -vel.getAtMACY(i - 1, bnd, bnd).y + dst(i - 1, bnd, bnd);



	//for (int i = bnd; i < dst.getSizeX() - bnd; ++i)
	//	for (int j = bnd + 1; j < dst.getSizeY() - bnd; ++j)
	//		dst(i, j, k) = vel.getAtMACX(i, j - 1, k).x + dst(i, j - 1, k);

	//for (int j = bnd + 1; j < dst.getSizeY() - bnd; ++j)
	//	dst(bnd, j, k) = -vel.getAtMACX(bnd, j - 1, k).x + dst(bnd, j - 1, k);

	//for (int i = bnd+1; i < dst.getSizeX()-bnd; ++i)
	//	dst(i, bnd, k) = -vel.getAtMACY(i-1, bnd, k).y + dst(bnd, bnd, k);

	//for (int i = bnd+1; i < dst.getSizeX()-bnd; ++i)
	//	for (int j = bnd + 1; j < dst.getSizeY() - bnd; ++j)
	//		dst(i, j, k) = -vel.getAtMACX(i, j - 1, k).x + dst(i, j - 1, k);

	//for (int j = bnd+1; j < dst.getSizeY()-bnd; ++j)
	//	dst(bnd, j, k) = -vel.getAtMACX(bnd, j - 1, k).x + dst(bnd, j - 1, k);
}

//! Get stream function
PYTHON() void getStreamfunction(FlagGrid* flags, MACGrid* vel, GridBase* grid)
{
	// determine type of grid    
	assertMsg(grid->getType() & GridBase::TypeReal, "getStreamFunction: takes only real grid");

	// dimension of grid
	if (!grid->is3D()) {
		fnStreamFunction2D(*flags, *vel, *((Grid<Real>*) grid));
	}
	else 
		fnStreamFunction3D(*flags, *vel, *((Grid<Real>*) grid));

}

////! Get velocity from stream function
//PYTHON() void getVelocityFromStreamfunction(FlagGrid* flags, GridBase* grid, MACGrid* vel)
//{
//	// determine type of grid    
//	assertMsg(grid->getType() & GridBase::TypeReal, "getVelocityFromStreamfunction: takes only real grid");
//
//	// dimension of grid
//	if (!grid->is3D()) {
//		fnVelocityFromStreamFunction2D(*flags, *((Grid<Real>*) grid), *vel);
//	}
//	else
//		errMsg("getStreamfunction: supports only 2d for now");
//
//}

} // namespace